package com.turo.clasevolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView result;
    Button btn;
    Requests requests = new Requests(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = findViewById(R.id.result);
        btn = findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Don't forget to add
                    <uses-permission android:name="android.permission.INTERNET"/>
                to the AndroidManifest.xml
                */
                requests.myRequest(new Requests.VolleyCallback() {
                    @Override
                    public void onSuccess(String resp) {
                        Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        result.setText("Result goes here:\n" + resp);
                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        result.setText("Result goes here:\n" + error);
                    }
                });
            }
        });

    }
}

