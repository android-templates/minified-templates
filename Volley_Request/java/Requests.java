package com.turo.clasevolley;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

// A class to manage all the request necessary for the App
public class Requests {
    public Context mContext;
    private static final String TAG = "Requests";
    public Requests(Context context) {
        mContext = context;
    }

    // Interfaces provide a way to keep coding with data from this class in another context
    public interface VolleyCallback {
        void onSuccess(String resp);
        void onFailure(String error);
    }

    public void myRequest(/*final String param1, final String param2, */final VolleyCallback callback) {
        // Here goes the url of your endpoint
        String url = "https://api.github.com/";
        // Remember to specify the Method you will use
        int  requestMethod = Request.Method.GET;

        StringRequest stringRequest = new StringRequest(requestMethod, url, new Response.Listener<String>() {
            /*
            The onSuccess() and onFailure() methods from the interface will declare what will
            happen with the response or error message in another context. In this case, that
            context is the main Activity
            */
            @Override
            public void onResponse(String response) {
                callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFailure(error.toString());
            }
        }){
            @Override
            /*
            Here you declare the headers of the request. Headers are data that explain about the
            contents of the request.
            */
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("User-Agent","android");
                headers.put("Content-Type","application/json");
                return headers;
            }

//            // This is for POST requests
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<>();
//                params.put("param_1",param1);
//                params.put("param_"",param2);
//                return params;
//            }
        };

        // After everything is ready, add the request to the Volley's Request Queue
        Singleton.getInstance(mContext).addToRequestQueue(stringRequest);
    }
}

