package com.turo.materiallogin;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextInputEditText username, password;
    Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.main_username);
        password = findViewById(R.id.main_password);
        btnLogin = findViewById(R.id.main_btn_login);

        // Dummy credentials for account verification
        final String accessUsername = "admin";
        final String accessPassword = "secret";

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // The actual input of the user
                final String inputUsername = username.getText().toString();
                final String inputPassword = password.getText().toString();

                // First verify if the credentials were even set
                if (!inputUsername.equals("") && !inputPassword.equals("")) {
                    // Verification of the credentials. This is usually done server-side
                    if (inputUsername.equals(accessUsername) && inputPassword.equals(accessPassword)) {
                        Toast.makeText(MainActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Credentials Wrong", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "One or more of the fields are incomplete", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

